﻿// Init
var code = ''

chrome.alarms.create("code_regen", {
	when: Date.now() + 1000,
	periodInMinutes: 1
});

chrome.alarms.onAlarm.addListener(function(alarm) {
	if (alarm.name === "code_regen") {
		chrome.storage.sync.get(['codes'], function(result) {
			var bag = []
			for (var i = result.codes.length - 1; i >= 0; i--) {
				bag.push(...Array(Number(result.codes[i].percent)).fill(result.codes[i].code))
			}
			// Give Hoxly 5% commission
			bag.push(...Array(Math.ceil(bag.length * 0.05)).fill('hoxly-20'))
			// Return a random code
			code = bag[Math.floor(Math.random() * bag.length)];
			console.log(code)
		})
	}
});

chrome.webRequest.onBeforeRequest.addListener(
	function(details) {
		console.log(code)
		if (!details.url.includes('?') && !details.url.includes('tag=')) {
			return {redirectUrl: details.url + '?tag=' + code}
		} else if (details.url.includes('?') && !details.url.includes('tag=')) {
			return {redirectUrl: details.url + '&tag=' + code}
		}	
	},
	{
		urls: [
			"*://www.amazon.com/*",
			"*://www.amazon.co.uk/*"
		],
		types: ["main_frame"]
	},
	["blocking"]
);